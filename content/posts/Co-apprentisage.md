---
title: "Co Apprentisage"
date: 2021-11-14T22:33:55Z
draft: false
tags: ["Education", "Essaie"]
---

# Introduction

Cet article de blog n'a aucune prétention. Je le réalise suite à une constatation durant mes heures de tutorats avec mes élèves de 1er année de BUT.

J'y propose des solutions et des aménagement possible à faire en cours afin de faciliter l'assimilation des notions d'algorithmie à de nouveaux élèves (principalement lycée et post-bac) découvrant le monde fascinant de la programmation.


Ce sont juste des idées lancé sur le "papier" et elles n'ont pas la prétention d'êtres **LES** solutions ultime, mais juste des éléments, des pistes de réflexions.


N'hésite pas à me contacter via : [delplace.nicolas@protonmail.com](mailto:delplace.nicolas@protonmail.com) si vous avez la moindre question ou remarque sur ce billet de blog.

# Le constat :

L'IUT A de l'université de Lille propose à ses élèves de 1re année de BUT informatique en difficulté des cours de soutien appelée : *tutorat*.

Ce sont des anciens élèves de l'IUT encore en étude à l'université de Lille qui viennent gérer un groupe de 3 ou 4 élèves afin de les aider.

Dans mon cas (un peu spécial) j'ai une dizaine d'élèves et cela se rapproche plus d'un cours que du tutorat, à la différence qu'il n'y a pas la relation profs-élève et que la gestion de classe est plus "souple." (ils peuvent communiquer, manger, etc...)

C'est durant les moments d'explications/rappel de cours que j'ai remarqué quelque chose à la fois étrange et comique.

J'explique la notion, je demande si tous est ok pour tout le monde, ils acquiessent tous (même si quelque sourcils sont froncés) et ils se lancent tous sans attendre sur leurs claviers/papiers.
Mais 1 à 2 min après les premières têtes se relèvent pleines de doute et au lieu de me poser une question, ils chuchottent avec leurs voisins pour leur demander quelque chose.

Généralement, les réponses du camarade à la question sont hasardeuses, compliquées voire même des fois un peu à côté de la plaque **MAIS** pourtant ils arrivent à se comprendre et à respecter la consigne.

**Mon constat :**
Les élèves en s'entraidant ont moins de gêne, posent plus de questions, avancent plus vite.

# Idée :

Favoriser l'apprentisage des notions algorithmique en créant des binome d'entraide.

Les classes seraient diviser en deux.
* Un demi-groupe avec l'enseignant répondant aux questions 
* L'autre demi-groupe en autonomie réaliseront des TP/td en binômes.

La classe pourrait êtres séparé par une baie vitré ou un volet coullisant (comme les portes japonnaise) et ainsi éviter la gêne visuelle et auditive pour chacun des groupes.

# Les aventages : 
## Point de vue de l'enseignant :
> Diviser pour mieux régner 

L'élève étant censé arriver en TD/TP pour mettre en pratique les notions qu'il a appris en cours avant, l'enseignant n'est pas censé durant ce cours leurs expliquer les notions mais juste êtres la pour répondre à des questions, doutes ou incrompréhension sur certain point de la notion.

Les critiques qu'on peut entendre sur les demi-groupes [comme-ici](https://www.cahiers-pedagogiques.com/%c2%bd-%c2%bd/) ne sont pas forcement justifié dans notre cas.

En effet l'enseignant aura plus de temps pour répondre au questions des élèves.

Les même élèves étant en demi-groupe auront moins de pression/jugement social venant des autres et pourons ainsi poser plus de questions.

Pour l'enseignant c'est du 100% gagnant. Il a plus de temps pour ses élèves, gestion de classe plus simple, meilleur vision du niveau global de la classe et de chaque élève.

## Point de vue des élèves :
### Pour les élèves en autonomie :

Pour les élèves en autonomie, ils seront pas tous de suite mit en binome, pendants les premières scéance on voit le niveaux de chacun et une fois cela fait crée des binomes de niveaux et d'affinité afin qu'un élève pousse l'autre vers le haut.

Une des critiques éventuelle pourrait-êtres : "Oui mais le nul va empecher le bon de travailler".

Alors OUI .... mais NON.

L'élève possant la question travaillera sa logique de décomposition de problème, reformulation (possibilité de trouver la réponse seul cf : [methode du canard](https://fr.wikipedia.org/wiki/M%C3%A9thode_du_canard_en_plastique))

L'élève répondant à la question lui devra travailler sa compréhension, son esprit de synthèse et surtout en expliquant quelque chose on voit ce qu'on a compris ou non (cf : [méthode de Feynman](https://everlaab.com/la-methode-dapprentissage-de-feynman/))

En sommes les deux élèves sont gagnant et apprenent l'un de l'autre.

**Attention néanmoins** il ne faut pas qu'un élèves fasse tous et que l'autre ne fasse que récupérer le fichier.

Pour éviter cela plusieurs méthode :
* Une petite évalution/QCM à chaque fin de cours pour voir la compréhension du binome et faire une moyennes des deux notes pour le binomes.
* Faire passer un binome au hasard à chaque cours et les noter
* Note QCM bonus (comme point 1) mais bonus si au dessus de 10.

Cela insitera les binômes à travailler car il y'a la fameuse "note".

### Pour les élèves avec le prof :
Plus facile de poser des questions car moins de doights levé, moins de jugement de la part de ses camarades de classe.

C'est donc un espace d'échange privilégié avec l'enseignant.

## Financier :

A part l'éventuelle financement pour les portes coulisante/vitre, c'est 2 fois moins de machine à acheter et un enseignant de tp en moins.

# Les inconvénients :

Cette méthode d'enseignemant est difficile à mettre en place.

En effet dans un établisement ou il y'a peut de soucis de comportement/travail cela ne posera aucun soucis, mais dans des établisement avec ce genre de soucis c'est la porte potentielle aux bêtisse et au cours "récrée".

Pour ce genre de classe à problème, la solution serait d'avoir un enseignant ou au moins un surveillant de présent dans la salle en autonomie. (ce qui ruine l'avantage économique).

Cela prend plus de temps au niveaux préparation des cours en TP/TD car cela implique de crée des QCM à chaque fois OU une perte de temps si options présentation en fin de cours.

Vue que le profs est pas derrière eux, ils faut blinder les codes de testes déjà fait pour voir si le code des élèves respecte bien les condition et prérequis (ce qui est un coût en temps non négligeable).


# Conclution :

C'est une méthode d'enseignement fortement utile, humaine et permet aux étudiants un gain d'autonomie et de les préparer aux travailler d'équipe en entreprise.

C'est sur le point pédagogique intéresant, mais d'un point de vue gestion cela semble plus compliqué.
C'est un coût matériel, de temps concidérable, surtout que cela dépendant énormement du type d'établisement et de classe à qui profitera de cette méthode.


C'est une méthodologie intéresante mais surement impossible de metre en place telle que je le décris (maladroitement ci-dessus), mais des élèments peuvent êtres prit je pense comme approche.

L'informatique est clairement le SEUL domaine ou je peux voire cette méthode applique t'elle qu'elle.

