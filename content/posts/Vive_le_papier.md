---
title: "Vive le papier"
date: 2021-11-25T14:41:57Z
draft: false
tags: ["Education", "Essaie"]
---

# Introduction

On verra dans ce petit article quel est donc le lien entre informatique et le papier, et pourquoi amha sont-ils liés.

J'écris ceci suite à une constatation durant mon 1er stage d'observation dans l'Éducation National.

Ce sont juste des idées lancé sur le "papier" et elles n'ont pas la prétention d'êtres **LES** solutions à tous les problèmes, mais uniquement des pistes de réflexion.

N'hésitez pas à me contacter via : [delplace.nicolas@protonmail.com](mailto:delplace.nicolas@protonmail.com) si vous avez la moindre question ou remarque sur ce billet de blog.

# Le constat :

J'ai durant ma 1er année de master MEEF 1 informatique dû réaliser des stages de deux semaines dans un lycée et voir comment est fait un cours de NSI et/ou de SNT dans la pratique.

Ce qui m'a le plus surpris (hormis les différences de niveaux énormes entre les élèves) c'est qu'aucun d'eux n'utilise de papier durant les TPs.

Chaque élève à un sujet numérique, un IDE pour son code et c'est tous... Personne n'a jamais sorti de feuille.

**Sauf** que pour ma part, je ne me vois pas coder sans brouillon, c'est l'outil de base d'un informaticien amha.

# Du papier en informatique ?

## Éviter les erreurs de conception/algo :
Et oui, pour moi l'outil de basses de tout bon informaticien, c'est une feuille de papier, ou tout du moins un brouillon (l'utilisation de paint comme brouillon par des streamers dev est fréquent - [preuve (3min23)](https://www.youtube.com/watch?v=01bVgkm34HE&t=61s)).

Ce même papier est d'une importance cruciale au début d'un tp, exo, car il nous permet de mettre sur papier nos idées et nos pistes de réflexions.

Car on a tous une idée de comment résoudre un problème en un algo, et on a tous tendance à partir comme des brutes directement dans le code !

On code pendant 5, 10, 15 voir 20 min, on compile et la ... **BIM** .... une erreur, puis 2, 3 et 4, car on n'a pas pensé a t'elle cas d'arrêt, ou à ce problème-là, etc etc ...

Le papier est **LE** remède contre ce problème car en posant sur le papier nos idées, on visualise plus facilement notre algo et on voit ainsi plus facilement nos erreurs de conception voire même de compréhension du problème.

## Meilleur compréhension :

Le brouillon sert aussi à la compréhension, il peut servir de support afin de faire des schémas, graphiques et autre test afin de voir (avant de coder) si on a bien compris l'exercice.

L'exemple bateau :
>>> Un escargot monte de 3 casses la journée et la nuit vu qu'il dort, retombe de deux... 
>>> 
>>> Combien de jour lui faut-il pour dépasser la 10eme casse ?

C'est un exercice tout simple, pas des maths très poussés hein ... et pourtant je suis quasiment sûr que vous avez compté sur vos doigts, fermé les yeux pour réfléchir, etc... À ce moment, une feuille et un crayon ne son pas nécessaire et pourtant bien utile.
## Une aide bien utile :

Le papier et la béquille du dév, elle lui permet de poser son raisonnement de manière plus graphique que des lignes de code.
Ce même petit morceau de papier lui permet aussi de mieux comprendre le problème et les différents cas d'arrêt...

C'est en soit un outil non pas nécessaire, mais je pense d'une aide plus que précieuse et notamment pour de jeunes voulant apprendre les joies du code.

# Mais comment faire utiliser du papier à des jeunes ?

## Leur fournir

Il y aura toujours un petit malin qui oubliera son bloc-note ou qui prétendra l'avoir oublié (comme la bonne vielle calculette).

Quoi de mieux que de leur fournir dans ce cas afin d'êtres sûr qu'ils en aient ?

## Durant les questions

Quand un élève pose une question, il faudrait lui donner un morceau de papier à côté de lui et lui demande de nous dessiner :
  - Sa logique de son algo
  - De nous donner un exemple (avec un cas simple et rapide).

À ce moment-là, l'élève trouvera sûrement par lui-même d'où vient son problème (erreurs de logique, mauvaise compréhension de l'exo, cas d'arrêt manqué, etc...) le pro lui ne sera là quand soutiens si l'élève ne trouve pas.

En plus d'êtres une bonne pratique cela permet à l'enseignant d'aider son élève sans pour autant le guider et éviter ainsi des fois de donner des indices trop évidemment.



# Conclusion :

Pour moi le papier en plus d'êtres un outil et une bonne pratique en informatique et un outil pédagogique important.

Comme expliqué dans la rubrique *Durant les questions*, c'est pour moi une bonne solution afin d'aider les élèves.

Car le souci en tant qu'enseignant, c'est de réussir à donner une assez bonne explication/indice afin de mettre sur la voie l'élève, sans pour autant lui en donner un trop gros et ainsi lui macher le travail.

Avec le papier, c'est l'élève qui fait 90% de ce travail. L'enseignant ne sera là que pour aide dans les 10% restants et en fonction de ce que l'élève à compris et su expliquer (bonne exo aussi pour le grand oral de NSI).

**Pour conclure** : La papier en informatique à encore de beaux jours devant lui.

-----------------

Data Love 

