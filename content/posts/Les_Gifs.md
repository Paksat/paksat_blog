---
title: "Les_Gifs"
date: 2021-11-25T14:41:57Z
draft: false
tags: ["Education", "Essaie"]
---

# Introduction

Cet article de blog n'a aucune prétention. Je le réalise afin de partager une idée qui m'est venue accoudé au bar avec des amis.

Ce sont juste des idées lancé sur le "papier" et elles n'ont pas la prétention d'êtres **LES** solutions à tous les problèmes, mais uniquement des pistes de réflexion.

Cet article plus que les autres n'est qu'un brouillon, une prémisse.

N'hésitez pas à me contacter via : [delplace.nicolas@protonmail.com](mailto:delplace.nicolas@protonmail.com) si vous avez la moindre question ou remarque sur ce billet de blog.

# Le constat :

Durant une soirée avec des amis, un débat arriva ... Celui de la prononciation du mot gif !

Doit-on dire "*Jif*", "*gifeux*", "*djif*", un véritable débat venait de naître, pour êtres conclus assez vite par un simple "On s'en fiche de comment ça se dit c'est bon qu'a répondre un truc quand tu sais pas quoi dire".

Et là, je tic ! En effet sur les pages Wiki les gifs sont commun, et celui du jeux de la vie m'avait bien aidé à mes débuts en programation ([page jeux de la vie](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life#/media/File:Gospers_glider_gun.gif))

Et je me dis à ce moment-là, "Les gifs pourraient-ils être de bons outils pédagogiques ?

# Les differens avantage du gif en informatique ?

## Légé et portable ! 

Nous avons souvent tendance à montrer des exemples vidéo, qui souvent son extrait d'une bande de vidéo en ligne (Youtube, PeerTube, etc) pouvant obliger les apprenants à avoir des écouteurs, faire des allers-retours dans la vidéo, prier pour ne pas voir la ressource disparaître ou que la plateforme bug à ce moment.
Et cela, quand nous avons la chance d'avoir un accès internet ou que les sites en questions de ne soient pas bloqués par la structure !

Alors qu'un gif, comme une image n'a besoin de rien d'autre qu'un navigateur pour êtres vue et utilisé, ils sont légers, simple d'utilisation et on ne peut pas vraiment jouer avec !

## Autonomie :

Un gif se répétant, il permet à l'apprenant d'avoir tout le temps qu'il lui faut pour acquérir la notion ou le problème.

Cela lui permet d'avoir un exemple comme pourrait faire l'enseignant au tableau certes, mais de revoir le procédé étape par étape et du début à la fin autant de fois qu'il lui faut pour comprendre.


# Désavantage :

Les gif ont néemoins des désavantage majeur !

Le premier étant qu'il est distrayant !

En effet, le gif bouge par essence et vue que cela attire l'œil, notre subconscient ne peut ignorer ce stimulus visuel et cela pollue notre attention.
Cela est encore plus vrai si le gif est en lancement automatique et en boucle.

## Pour éviter ça :
- Gif non lancé automatiquement, on doit faire le choix de le lancer.
- Si lancé, il finit par se mettre en pause après X sec et/ou itération.
    - Le fait de déscendre dans la page fait que la vidéo s'arrête et on doit par NOUS même décider de le relancer.
- Possibilité de faire des vitesses X2 ou X0.5 avec de itération progressuive au click.

# Conclusion :
**Pour conclure** :  Les gifs sont des outils de réflexion et d'éducation fort et pouvant permettre l'acquissions de notion en plus ou moins grande autonomie.

Ils ont certes des défauts, mais comme nous l'avons vue, il existe des solutions pour les attenues.
-----------------

Data Love 

