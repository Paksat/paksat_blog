---
title: "Auto Evaluation"
date: 2021-12-08T22:32:01Z
draft: false
---

**En cours d'écriture**

# Intro : 

Durant mon stage en lycée pour mon M1 MEEF info j'ai pu discuter avec mon tuteur d'un problème lié à l'évaluation de la matière informatique en NSI (notamment) :

> Comment évaluer les TPs ou les projets réalisés par les élèves ?

En effet, en "quasi"-autonomie, seul derrière un PC avec une connexion à Internet, rien de plus simple pour un élève que de tricher !

Le voisin qui donne son code (ou des parties), des bouts de code piqués à droite et à gauche sur Internet (quand ce n'est pas tout le code #StackOverFlow)...

Comment êtres-sur que ce que l'on évalue ce soit bien les connaissances et les compétences de l'élève et pas celle du voisin ou d'un illustre inconnue d'Internet ?

Je vais donc essayer de partager une solution : **l'auto-évaluation** !!

# L'idée :

Une solution serait de réaliser un QCM **non** noté portant sur des notions vue durant le(s) TP, voir même des questions sur la compréhension propres du TP.

Par exemple :
> Que sortira cette fonction avec les paramètres X et Y ? --> A : 42 | B = 69 | C = 4269 | D = D

Ou
> La méthode X("toto") et t'elle plus rapide que Y("toto") ? A : Oui | B = Faux.

Ce sont des questions simples, rapides permettant des les écrire facilement et rapidement.
Ce qui pourrait-êtres un plus :
- Mettre des rappels de cours si erreur.
- A la fin du QCM donner les liens vers les notions de cours où l'élève à eu faux.

Mais pourquoi je précise non noté ?

Car le but, c'est surtout pour l'élève de voir ce qu'il a compris ou non et ainsi pouvoir revoir la notion.

Je pense que si cela est noté, les élèves auront peur à chaque cours de ce QCM et le cours deviendra un moment angoissant pour toute la classe, où il attende le moment tant redouté du QCM et de sa note.

Cela sera d'autant plus vraie pour  les décrocheurs ou fragile sur les notions qui risque de ce démotive au fur et à mesure.

@

Néanmoins ! Le prof pourrait récupérer les réponses et en faire une note pour lui afin d'avoir un repérer plus facile et rapide à lire.

**MAIS** l'élève lui ne devra jamais avoir de note, mais uniquement des retours direct à ses réponses (bon, Faux) et ensuite revoir les notions où il a eu du mal.

# Les différents avantages: 

TODO

## Eviter les syndrome d'oubli post cours/exam
Bien connue des élèves le fameux : SOPP ou SO2P ...
>Syndrome d'oublie post partiel.
>
Ce syndrome fait qu'une fois sortit d'une épreuve, on oublie absolument TOUS ce qu'on a appris ou au moins 90%.

Ce triste syndrome existe aussi pour les cours...
D'une semaine sur l'autre, un élève souffre du SO2P, sauf si il revoit sont cours avant le cours.

Et c'est **LA** que le QCM entre en jeu.

### Le qcm contre l'effet SO2P :
Vue que l'élève va devoir répondre à un QCM soit :
- Va réviser : Dans ce cas le QCM lui permettra d'êtres un outils permettant de jaugé sa compréhension de la notion
- Ne révisera pas : Il verra durant les réponses fausse un rappel du cours et pourra à la fin du cours cliqué sur les liens des notions mal comprise et pourra ainsi les revoir ou savoir quoi réviser

C'est en soit un outils d'auto-évaluation des compétences, la note étant personnelle, non prises en compte dans la moyenne l'élève n'aura aucune pression (peur du jugement du profs/camarades/parents.)

## GAMIFICATION :

Mais si il n'y a aucune note et sanction du profs, l'élève pourrait ce dire :
> Je vais m'en débarrasser vite en répondant tous au pif, au pire je risque rien.

C'est en effet tout à fait possible, une solution possible, rendre cela ludique avec la GAMIFICATION.

Comme dans un jeu vidéo, les élèves auraient un personnage avec des caractéristique (vie, endurance, mana, etc..) et de l'expérience.

Pour passer un niveau supérieur et ainsi pouvoir avoir de meilleur statistique, ils leurs suffira de répondre au QCM.

Si il répond bon à un certain nombre de questions lié à une notion, alors il l'a débloqueront ce qui rapportera de l'XP et un badge visible dans un arbre de compétence : [comme ici](https://www.google.com/search?q=skyrim+arbres+de+comp%C3%A9tence&client=firefox-b-d&sxsrf=AOaemvL-etVklPUeBHVsl1mKD11Py76s5A:1639090606758&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjHjvqq6Nf0AhXFy4UKHYE9B0IQ_AUoAXoECAEQAw&biw=2400&bih=1159&dpr=0.8#imgrc=fqOoDegXj_ywRM)

Ce personnage propre à l'élève sera uniquement accessible sur sa machine avec un petit script python.
Je pense qu'il est préférable d'éviter de les mettre en commun pour éviter les tensions, et le retours d'une compétition (peut êtres favorable dans certain cas)

### Activité péda
Cette gamification peut avoir un grand potentielle pédagogique car rien n’empêche de donner certaine partit à codé au étudiant de tous niveau :
- SNT : Trouver dans le code de la page Perso.python ou sont les points de vie, changer la valeur pour 42
- 1ER NSI : réaliser des fonctions permettant l'ajout de compétences ou getter/setter.
- T NSI : changer le système de tableau pour une BDD.

C'est des sujets de TP ludique et qui s'inscrive dans le temps, ce sont des fichiers qui suivront dans le temps l'élève  et sa monté en connaissance.

C'est un moyen de voir une utilité concrétè du code et de rendre le code plus ludique.
### Progression 
Tous comme dans un jeu vidéo, on atteint pas la super compétence "Eclair noir de la mort qui tue" sans savoir utilisé un simple "Courant électrique", il faut suivre une progression.

Et bien dans le jeu pareil, il faudra maîtriser une notions pour passer à la suivante et ainsi débloque les levels suivant. 

Un élèves pourrait êtres level 99 en prog, mais 0 sur l’arbre réseau.

Il pourra donc reprendre de 0 les basses du réseau est ainsi monter petit à petit à son rythme (avec le prof OU seul). 
### Acteur :
Vue que les élèves pourraient êtres à la fois acteur du code et de leurs personnage cela pourrait les insister à s'investir un peut plus dans la matière

## Valorisation du travail


## Fiche de suivit automatique 
Tous est dans le nom, le profs (comme l'élève avec l'arbres de compétence/une fiche de suivit) pourra voir où l'élève est bon et la ou il a du mal.

Parfait pour savoir ou appuyer en cours pour le prof et ou réviser pour l'élève.
## Début VS Fin de cours ?

# Comment le présenter ?

# Conclution
