---
title: "Bienvenue"
date: 2021-11-14T21:23:23Z
draft: false
---


# Bienvenue à vous sur ce blog

## Pourquoi ce blog ?
J'ai au départ réalisé ce blog afin de m'amuser et d'apprendre à utiliser l'outil Hugo et le CI/CD de gitlab.

Au final, je pense utiliser ce blog afin de partager des idées, des réflexions et peut-être même des aides/fiches de révisions.

Toutes les idées que je formulerais sur ce blog seront des points de vue que j'aurais à un instant T et ne refléterons peut-être plus qui je suis ou ce que je pense dans les années à venir.

## Le futur du blog :
Je ne souhaite néanmoins pas supprimer ou modifier mes articles (ou le moins possible) afin de voir mon cheminement de pensé et permetre à d'autre de nourir leurs propres réflexions.

C'est ce que j'ai fait avec les [écrits d'Aaron Swartz](https://editions-b42.com/produit/celui-qui-pourrait-changer-le-monde-2/). On voit au fil des années ses réflexions, ses idées, ses convictions évoluer et cela est (je trouve) extrêmement intéressant.

Il y aura évidemment des textes qui seront *"périmés"* (le temps aura prouvé que j'ai eu tort, ajout de preuves qui contredisent mon idée, etc...) mais cela n'est pas grave, c'est le cycle naturel des choses et relire ce genre de texte permet aussi de construire sa réflexion, son esprit critique et bien d'autre.

En soit, ce blog n'a **AUCUNE** prétention et n'est là que pour me permettre de m'exprimer sur certains sujets et d'éventuellement en débattre avec vous :)

Aller, je vous dis à une prochaine sur l'internet mondial des ordinateurs interconnecté

Data love <3

